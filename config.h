/*
Copyright 2017 Danny Nguyen <danny@keeb.io>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

/**
 * https://docs.qmk.fm/#/tap_hold?id=tapping-term
 * Don't wait too long for me to hold.
 */
#define TAPPING_TERM 150

/**
 * https://docs.qmk.fm/#/tap_hold?id=permissive-hold
 * This goes hand in hand with IGNORE_MOD_TAP_INTERRUPT.
 */
#define PERMISSIVE_HOLD true

// Get rid of the defaults, why is it breathing by default anyway
#undef RGBLIGHT_DEFAULT_MODE
#define RGBLIGHT_DEFAULT_MODE RGBLIGHT_MODE_STATIC_LIGHT

#define EE_HANDS
