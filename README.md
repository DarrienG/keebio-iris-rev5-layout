# Darrien's layout for iris rev 5+6 (so standard layout)

A baby layout inspired by the full moonlander layout here:
https://blog.darrien.dev/posts/twolander-a-moonlander-update/ and intended to
mimic it as much as possible even with the -1 row to make the Iris a good
replacement keyboard while on the go.

# Differences

Despite one whole less row, the differences are minimal between the two.

## Layer 1

On the far left, control is now a mod_tap key on Z instead of being in its own
row. Keys 2 and 3 in the thumb cluster are gone so they cannot exist.

The emoji key on the big landing pad thumb cluster on the left has been replaced
by the very important layer key that takes its place so we can access more keys.

In the middle, + and = are now on a separate layer. A shame. Also on the right
the arrow keys are removed.

## Layer 2

Formerly just the media layer, L2 now serves the dual purpose of media layer and
+ and = keys. Otherwise everything else is still the same.

## Layer 3

Layer 3 is identical.

# Viewing

I'm not one to shill for Microsoft products, but unfortunately the best viewier
for keymap.c is the great keymap.c editor/viewer for VSCode:
https://marketplace.visualstudio.com/items?itemName=Ciantic.keymapceditor-vsc

To open, do CMD + SHIFT + P: Show KeymapCEditor Keymapc file.

Then pick in the configuration options:
* Keyboard -> Iris
* Layout -> US Keyboard

# Compiling and flashing

Make sure you have qmk installed `brew install qmk` and you've run setup `qmk
setup`

Once that's done it's an easy peazy `qmk compile`

Compiled assets will be in your setup folder and ready to flash with the qmk
toolbox.

# Pretty pictures of the layouts

Layer1:
![layer 1](/assets/l1.png)

Layer2:
![layer 2](/assets/l2.png)

Layer3:
![layer 3](/assets/l3.png)

# Helpful resources

Cheatsheet: https://jayliu50.github.io/qmk-cheatsheet/

Working with the source code: https://docs.qmk.fm/#/newbs_building_firmware?id=customize-the-layout-to-your-liking

Proper way of building:
Poke around in [qmk configurator](https://config.qmk.fm/#/) for changes, then
it's go time.

```bash
# Setup - only once
# Make sure qmk is installed!
git clone https://gitlab.com/DarrienG/keebio-iris-rev5-layout.git ~/qmk_firmware/keyboards/keebio/iris/keymaps/darrien-standard

# Every time afterwards
$ qmk json2c rev6adefault.json > keymap.c
# You might use something other than rev6a - iris layouts are swappable!
$ qmk compile -kb keebio/iris/rev6a -km darrien-standard-iris
# Note the last hex file after the compile and flash with qmk toolbox
```

# Extra notes
Currently the keymap.c and rev6adefault.json are out of sync - that's ok.
In the JSON you can't modify things like colors of the keyboard as a whole.

The keymap.c includes an extra ifdef for turning the backlight white.
If you modify the JSON, consider reapplying this if you want the backlight to be
white as well. Great for frosted plates, not so good for see through. Otherwise
lighting is red.
